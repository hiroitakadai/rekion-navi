<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Builder;

/**
 * App\History
 *
 * @property int $id
 * @property int $user_id
 * @property int $rekion_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\History newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\History newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\History query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\History whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\History whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\History whereRekionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\History whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\History whereUserId($value)
 * @mixin \Eloquent
 */
class History extends Model
{
    protected $guarded = ["id"];

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope('order', function (Builder $builder) {
            $builder->orderBy('created_at', "desc");
        });
    }

    public function rekion()
    {
        return $this->belongsTo("App\Rekion");
    }

    public static function createHistory(int $rekion_id)
    {
        $user_id = 0;
        if (Auth::check()) {
            $user_id = Auth::id();
        }
        History::create(["user_id" => $user_id, "rekion_id" => $rekion_id]);
    }
}
