<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;

class PasswordResetController extends Controller
{
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $user = User::where("email", $request->email)->first();
            if (!$user || !Hash::check($request->token, $user->reset_digest)) {
                return redirect('/');
            }
            return $next($request);
        })->only(["edit", "update"]);
        $this->middleware(function ($request, $next) {
            $user = User::where("email", $request->email)->first();
            if ($user->checkExpiration()) {
                session()->flash('message', ['danger' => 'パスワード更新の期限が切れています。']);
                return redirect()->back();
            }
            return $next($request);
        })->only(["edit", "update"]);
    }

    public function create()
    {
        return view("password_reset.create");
    }

    public function store(Request $request)
    {
        $user = User::where("email", strtolower($request->email))->first();
        if (!$user) {
            session()->flash('message', ['danger' => '登録されていないメールアドレスです。']);
            return redirect()->back();
        }
        $reset_token = str_random(22);
        $user->reset_digest = bcrypt($reset_token);
        $user->reset_sent_at = Carbon::now();
        $user->save();
        $user->sendPasswordResetMail($reset_token);
        session()->flash('message', ['info' => 'パスワードリセットのメールを送信しました。']);
        return redirect("/home");
    }

    public function edit(Request $request)
    {
        $user = User::where("email", strtolower($request->email))->first();
        return view("password_reset.edit")->with(["user" => $user, "token" => $request->token]);
    }

    public function update(Request $request)
    {
        $request->validate([
            'password' => 'required|min:6|confirmed',
            'password_confirmation' => 'required|min:6',
        ]);

        $user = User::where("email", strtolower($request->email))->first();
        $user->password = bcrypt($request->password);
        $user->reset_digest = null;
        $user->save();
        Auth::login($user);
        session()->flash('message', ['success' => 'パスワードを更新しました。']);
        return redirect()->route("user.show", $user->id);
    }
}
