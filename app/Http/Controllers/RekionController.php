<?php

namespace App\Http\Controllers;

use App\History;
use App\Rekion;
use App\Traits\PaginatorTrait;
use Illuminate\Http\Request;

class RekionController extends Controller
{
    use PaginatorTrait;

    public function index(Request $request)
    {
        if (!$request->q && !$request->tag) {
            //　空のEloquentを返す
            $query = Rekion::where("id", "0");
            $rekions = $this->paginate($query, 20, ['rekions.id']);
            return view("rekion.index")->with("rekions", $rekions);
        }

        $query = Rekion::query()
            ->select("rekions.*")
            ->join("rekion_tag", "rekion_tag.rekion_id", "rekions.id")
            ->groupBy("rekions.id");

        if ($request->q) {
            $query->join("tags", "tags.id", "=", "rekion_tag.tag_id");
            $keywords = preg_split("/\s+/", $request->q);

            $query->orWhere(function ($query) use ($keywords) {
                foreach ($keywords as $keyword) {
                    $query->where(function ($query) use ($keyword) {
                        $query->orWhere("rekions.title", "like", "%{$keyword}%")
                            ->orWhere("rekions.artist", "like", "%{$keyword}%")
                            ->orWhere("rekions.maker", "like", "%{$keyword}%")
                            ->orWhere("tags.name", "like", "%{$keyword}%");
                    });
                }
            });
        }

        if ($request->tag) {
            $query->orWhere("rekion_tag.tag_id", "=", $request->tag);
        }

        $rekions = $this->paginate($query, 20, ['rekions.id']);
        $request->flash();

        return view("rekion.index")->with("rekions", $rekions);
    }

    public function show(Rekion $rekion)
    {
        History::createHistory($rekion->id);
        return view("rekion.show")->with("rekion", $rekion);
    }
}
