<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class SessionController extends Controller
{
    public function create()
    {
        if (User::find(1)) {
            Auth::login(User::find(1));
            return redirect()->route("user.show", 1);
        }
        return view("session.create");
    }

    public function store(Request $request)
    {
        $user = User::where("email", strtolower($request->email))->first();
        if ($user && Hash::check($request->password, $user->password)) {
            Auth::login($user, $request->remember_me === "1");
            return redirect()->intended(route("user.show", $user->id));
        } else {
            session()->flash('message', ['danger' => 'メールアドレスがパスワードが正しくありません。']);
            return back()->withInput();
        }
    }

    public function destroy()
    {
        Auth::logout();
        return redirect("/home");
    }
}
