<?php

namespace App\Http\Controllers;

use App\History;
use App\Rekion;
use App\Favorite;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class StaticPageController extends Controller
{
    public function home()
    {
        $rekion_ranking = DB::table("histories")
                        ->select(DB::raw("count(*) as view_count, rekions.*"))
                        ->join("rekions", "histories.rekion_id", "=", "rekions.id")
                        ->groupBy("rekions.id")
                        ->orderBy("view_count", "desc")
                        ->take(5)
                        ->get();

        $recent_histories = History::select("rekion_id", DB::raw('MAX(created_at) As created_at'))
                            ->groupBy("rekion_id")
                            ->take(5);
        $rekion_histories = Rekion::joinSub($recent_histories, "recent_histories", function ($join) {
            $join->on("rekions.id", "=", "recent_histories.rekion_id");
        })->get();

        $recent_favorites = Favorite::select("rekion_id", DB::raw('MAX(created_at) As created_at'))
                ->groupBy("rekion_id")
                ->take(5);
        $rekion_favorites = Rekion::joinSub($recent_favorites, "recent_favorites", function ($join) {
            $join->on("rekions.id", "=", "recent_favorites.rekion_id");
        })->get();
        return view("static_page.home")->with(["rekion_ranking" => $rekion_ranking,
                                               "rekion_histories" => $rekion_histories,
                                               "rekion_favorites" => $rekion_favorites]);
    }

    public function about()
    {
        return view("static_page.about");
    }
}
