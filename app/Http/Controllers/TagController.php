<?php

namespace App\Http\Controllers;

use App\Rekion;
use App\Tag;
use Illuminate\Http\Request;

class TagController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->only(["store", "destroy"]);
        $this->middleware("ajax")->only(["index", "store", "destroy"]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $request->validate([
            "rekion_id" => "required|exists:rekions,id",
        ]);

        $rekion = Rekion::find($request->rekion_id);
        return view("tag.index")->with("tags", $rekion->tags);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            "name" => "required|max:20",
            "rekion_id" => "required|exists:rekions,id"
        ]);

        $tag = Tag::firstOrCreate(["name" => $request->name]);
        $rekion_tags = Rekion::find($request->rekion_id)->tags();

        if ($rekion_tags->count() < 10 && !$rekion_tags->find($tag->id)) {
            $tag->rekions()->attach($request->rekion_id);
        }

        return redirect()->route("tag.index", ["rekion_id" => $request->rekion_id]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Tag  $tag
     * @return \Illuminate\Http\Response
     */
    public function show(Tag $tag)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Tag  $tag
     * @return \Illuminate\Http\Response
     */
    public function edit(Tag $tag)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Tag  $tag
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Tag $tag)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Tag  $tag
     * @return \Illuminate\Http\Response
     */
    public function destroy(Tag $tag, Request $request)
    {
        $request->validate([
            "rekion_id" => "required|exists:rekions,id",
        ]);

        $tag->rekions()->detach($request->rekion_id);
        return redirect()->route("tag.index", ["rekion_id" => $request->rekion_id]);
    }
}
