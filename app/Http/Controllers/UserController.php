<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

class UserController extends Controller
{
    public function index()
    {
        $users = User::paginate(30);
        return view("user.index")->with("users", $users);
    }

    public function create()
    {
        return view("user.create");
    }

    public function store(Request $request)
    {
        $request->email = strtolower($request->email);
        $request->validate([
            "name" => "required|max:50",
            "email" => "required|max:255|email|unique:users",
            "password" => "required|confirmed|min:6",
            "password_confirmation" => "required|min:6"
        ]);
        $user = new User;
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = bcrypt($request->password);
        $user->save();
        Auth::login($user);
        session()->flash('message', ['success' => '歴音ナビにようこそ！']);
        return redirect("/home");
    }

    public function show(User $user)
    {
        return view('user.show')->with(['user' => $user]);
    }

    public function edit(User $user)
    {
        $this->authorize("edit", $user);

        return view('user.edit')->with('user', $user);
    }

    public function update(User $user, Request $request)
    {
        $this->authorize('edit', $user);

        $request->email = strtolower($request->email);
        $request->validate([
            "name" => "required|max:50",
            "email" => ['required', 'max:255', 'email', Rule::unique('users')->ignore(Auth::id())],
            "password" => "nullable|confirmed|min:6",
            "password_confirmation" => "nullable|min:6"
        ]);
        $user->name = $request->name;
        $user->email = $request->email;
        if (!empty($request->password)) {
            $user->password = bcrypt($request->password);
        }
        $user->save();
        session()->flash("message", ['success' => 'プロフィールを更新しました。']);
        return redirect()->route("user.show", $user->id);
    }

    public function destroy(User $user)
    {
        $this->authorize('delete', $user);

        $user->delete();
        session()->flash("message", ["success" => "ユーザーを削除しました。"]);
        return redirect()->route("user.index");
    }
}
