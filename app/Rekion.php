<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Rekion
 *
 * @property int $id
 * @property string $pid
 * @property string $title
 * @property string|null $artist
 * @property string|null $maker
 * @property string|null $publicated_at
 * @property string|null $production_number
 * @property string|null $genre
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Rekion newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Rekion newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Rekion query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Rekion whereArtist($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Rekion whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Rekion whereGenre($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Rekion whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Rekion whereMaker($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Rekion wherePid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Rekion whereProductionNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Rekion wherePublicatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Rekion whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Rekion whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property string|null $url
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Comment[] $comments
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Tag[] $tags
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Rekion whereUrl($value)
 */
class Rekion extends Model
{
    protected $guarded = ["id"];

    public function comments()
    {
        return $this->hasMany("App\Comment");
    }

    public function tags()
    {
        return $this->belongsToMany("App\Tag");
    }

    public function getViewCount()
    {
        return History::where("rekion_id", $this->id)->count();
    }

    public function getCommentCount()
    {
        return Comment::where("rekion_id", $this->id)->count();
    }

    public function getFavoriteCount()
    {
        return Favorite::where("rekion_id", $this->id)->count();
    }
}
