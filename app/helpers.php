<?php

if (! function_exists('full_title')) {
    function full_title($page_title = '')
    {
        $base_title = "歴音ナビ";
        if (empty($page_title)) {
            return $base_title;
        } else {
            return $page_title . " | " . $base_title;
        }
    }
}

if (! function_exists('gravatar_for')) {
    function gravatar_for($user, $options = ["size" => 80])
    {
        $gravatar_id = md5(strtolower($user->email));
        $gravatar_url = "https://secure.gravatar.com/avatar/{$gravatar_id}?s={$options["size"]}";
        return Html::image($gravatar_url, $user->name, ["class" => "d-flex mr-3"]);
    }
}

if (! function_exists('time_ago_in_words')) {
    function time_ago_in_words($date)
    {
        return \Carbon\Carbon::parse($date)->diffForHumans();
    }
}

if (! function_exists('dom')) {
    function dom($html)
    {
        $dom = new Symfony\Component\DomCrawler\Crawler();
        $dom->addHTMLContent($html, "UTF-8");
        return $dom;
    }
}

if (! function_exists('getGenreList')) {
    function getGenreList()
    {
        $genre_collection = \DB::table("rekion_tag")
            ->select(\DB::raw("count(*) as rekion_count, tags.*"))
            ->where("tag_id", "<=", 100)
            ->where("parent_tag_id", "=", null)
            ->join("tags", "rekion_tag.tag_id", "=", "tags.id")
            ->groupBy("tags.id")
            ->orderBy("tags.id")
            ->get();
        $genres = collect();
        foreach ($genre_collection as $item) {
            $genre = collect();
            $genre->name = $item->name;
            $genre->rekion_count = $item->rekion_count;
            $genre->id = $item->id;

            $genres->push($genre);
        }

        return $genres;
    }
}
