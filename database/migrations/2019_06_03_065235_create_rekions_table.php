<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRekionsTable extends Migration
{
    public function up()
    {
        Schema::create('rekions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string("pid");
            $table->string("title");
            $table->string("artist")->nullable();
            $table->string("maker")->nullable();
            $table->string("publicated_at")->nullable();
            $table->string("production_number")->nullable();
            $table->string("url")->nullable();
            $table->string("genre")->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('rekions');
    }
}
