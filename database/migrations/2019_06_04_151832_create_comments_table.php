<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommentsTable extends Migration
{
    public function up()
    {
        Schema::create('comments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer("rekion_id");
            $table->text("text");
            $table->integer("user_id");
            $table->timestamps();

            $table->index("rekion_id");
            $table->index("user_id");
        });
    }

    public function down()
    {
        Schema::dropIfExists('comments');
    }
}
