<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFavoritesTable extends Migration
{
    public function up()
    {
        Schema::create('favorites', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer("user_id");
            $table->integer("rekion_id");
            $table->timestamps();

            $table->index("user_id");
            $table->index("rekion_id");

            $table->foreign("user_id")->references("id")->on("users")->onDelete("cascade");
            $table->foreign("rekion_id")->references("id")->on("rekions")->onDelete("cascade");
        });
    }

    public function down()
    {
        Schema::dropIfExists('favorites');
    }
}
