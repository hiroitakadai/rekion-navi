<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHistoriesTable extends Migration
{
    public function up()
    {
        Schema::create('histories', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer("user_id");
            $table->integer("rekion_id");
            $table->timestamps();

            $table->index("user_id");
        });
    }

    public function down()
    {
        Schema::dropIfExists('histories');
    }
}
