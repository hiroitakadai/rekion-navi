<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRekionTagTable extends Migration
{
    public function up()
    {
        Schema::create('rekion_tag', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer("rekion_id");
            $table->integer("tag_id");
            $table->timestamps();

            $table->index("rekion_id");
            $table->index("tag_id");
            $table->index(["rekion_id", "tag_id"]);

            $table->foreign("rekion_id")->references("id")->on("rekions")->onDelete("cascade");
            $table->foreign("tag_id")->references("id")->on("tags")->onDelete("cascade");
        });
    }

    public function down()
    {
        Schema::dropIfExists('rekion_tag');
    }
}
