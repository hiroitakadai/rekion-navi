<?php

use App\Rekion;
use App\Tag;
use App\RekionTag;
use Illuminate\Database\Seeder;

class RekionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $i = 0;
        $read_file_path = __DIR__.'./../../storage/rekion_internet_20180921.csv';
        $file = new SplFileObject($read_file_path, 'r');
        $file->setFlags(SplFileObject::READ_CSV | SplFileObject::SKIP_EMPTY | SplFileObject::READ_AHEAD);

        foreach ($file as $row) {
            if (config('app.env') === 'production' && $i > 1000) {
                break;
            }

            $rekion = Rekion::firstOrNew(["pid" => substr($row[6], 42)]);
            $rekion->title = $row[1];
            $rekion->artist = $row[2];
            $rekion->maker = $row[3];
            $rekion->publicated_at = $row[4];
            $rekion->production_number = $row[0];
            $rekion->url = $row[6];
            $rekion->genre = $row[7];
            $rekion->save();

            $small_genre = null;

            if (preg_match('/^(\S+)\|\|統制語：(\S+)$/', $rekion->genre, $m)) {
                $tag = Tag::firstOrCreate(["name" => $m[1], "deletable" => false]);
                $rekion->tags()->attach($tag->id);
                $small_genre = Tag::where("name", $m[2])->first();;
            } elseif (preg_match('/統制語：(\S+)$/', $rekion->genre, $m)){
                $small_genre = Tag::where("name", $m[1])->first();;
            }

            if (!$small_genre) {
                $small_genre = Tag::find(100);
            }

            $rekion->tags()->attach($small_genre->id);
            $rekion->tags()->attach($small_genre->parent_tag_id);

            $i += 1;
        }
    }
}
