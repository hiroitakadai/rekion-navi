<?php

use App\Tag;
use Illuminate\Database\Seeder;

class TagsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tag = Tag::create(["name" => "器楽（クラシック音楽）", "deletable" => false]);
        Tag::create(["name" => "独奏、二重奏（オルガン）", "deletable" => false, "parent_tag_id" => $tag->id]);
        Tag::create(["name" => "独奏、二重奏（ピアノ）", "deletable" => false, "parent_tag_id" => $tag->id]);
        Tag::create(["name" => "独奏、二重奏（ヴァイオリン）", "deletable" => false, "parent_tag_id" => $tag->id]);
        Tag::create(["name" => "独奏、二重奏（チェロ）", "deletable" => false, "parent_tag_id" => $tag->id]);
        Tag::create(["name" => "独奏、二重奏（オーボエ）", "deletable" => false, "parent_tag_id" => $tag->id]);
        Tag::create(["name" => "独奏、二重奏（フルート、ピッコロ）", "deletable" => false, "parent_tag_id" => $tag->id]);
        Tag::create(["name" => "独奏、二重奏（クラリネット）", "deletable" => false, "parent_tag_id" => $tag->id]);
        Tag::create(["name" => "独奏、二重奏（トランペット、コルネット）", "deletable" => false, "parent_tag_id" => $tag->id]);
        Tag::create(["name" => "合奏（室内楽）", "deletable" => false, "parent_tag_id" => $tag->id]);
        Tag::create(["name" => "合奏（管弦楽）", "deletable" => false, "parent_tag_id" => $tag->id]);
        $tag = Tag::create(["name" => "器楽（クラシック以外）", "deletable" => false]);
        Tag::create(["name" => "器楽", "deletable" => false, "parent_tag_id" => $tag->id]);
        Tag::create(["name" => "独奏、二重奏（バンジョー）", "deletable" => false, "parent_tag_id" => $tag->id]);
        Tag::create(["name" => "独奏、二重奏（ギター）", "deletable" => false, "parent_tag_id" => $tag->id]);
        Tag::create(["name" => "独奏、二重奏（マンドリン）", "deletable" => false, "parent_tag_id" => $tag->id]);
        Tag::create(["name" => "独奏、二重奏（木琴）", "deletable" => false, "parent_tag_id" => $tag->id]);
        Tag::create(["name" => "独奏、二重奏（アコーディオン）", "deletable" => false, "parent_tag_id" => $tag->id]);
        Tag::create(["name" => "独奏、二重奏（ハーモニカ）", "deletable" => false, "parent_tag_id" => $tag->id]);
        Tag::create(["name" => "合奏（一般）", "deletable" => false, "parent_tag_id" => $tag->id]);
        Tag::create(["name" => "合奏（弦楽合奏、ギター合奏）", "deletable" => false, "parent_tag_id" => $tag->id]);
        Tag::create(["name" => "合奏（吹奏楽）", "deletable" => false, "parent_tag_id" => $tag->id]);
        Tag::create(["name" => "合奏（マンドリン、ハーモニカその他）", "deletable" => false, "parent_tag_id" => $tag->id]);
        Tag::create(["name" => "鼓笛隊用音楽、野外音楽", "deletable" => false, "parent_tag_id" => $tag->id]);
        Tag::create(["name" => "軽音楽", "deletable" => false, "parent_tag_id" => $tag->id]);
        Tag::create(["name" => "舞踊曲・ダンス音楽", "deletable" => false, "parent_tag_id" => $tag->id]);
        Tag::create(["name" => "ハワイ音楽", "deletable" => false, "parent_tag_id" => $tag->id]);
        $tag = Tag::create(["name" => "声楽（大分類）", "deletable" => false]);
        Tag::create(["name" => "声楽", "deletable" => false, "parent_tag_id" => $tag->id]);
        Tag::create(["name" => "声楽（合唱）", "deletable" => false, "parent_tag_id" => $tag->id]);
        Tag::create(["name" => "声楽（独唱）", "deletable" => false, "parent_tag_id" => $tag->id]);
        $tag = Tag::create(["name" => "民謡・国民音楽（外国）", "deletable" => false]);
        Tag::create(["name" => "民謡、国民音楽（世界）", "deletable" => false, "parent_tag_id" => $tag->id]);
        $tag = Tag::create(["name" => "民謡・国民音楽（日本）", "deletable" => false]);
        Tag::create(["name" => "民謡、国民音楽（日本）", "deletable" => false, "parent_tag_id" => $tag->id]);
        Tag::create(["name" => "国歌、祝祭歌", "deletable" => false, "parent_tag_id" => $tag->id]);
        Tag::create(["name" => "学生歌、校歌、寮歌、応援歌", "deletable" => false, "parent_tag_id" => $tag->id]);
        Tag::create(["name" => "団体歌", "deletable" => false, "parent_tag_id" => $tag->id]);
        Tag::create(["name" => "労働歌", "deletable" => false, "parent_tag_id" => $tag->id]);
        Tag::create(["name" => "軍歌", "deletable" => false, "parent_tag_id" => $tag->id]);
        $tag = Tag::create(["name" => "唱歌・童謡", "deletable" => false]);
        Tag::create(["name" => "唱歌、童謡", "deletable" => false, "parent_tag_id" => $tag->id]);
        $tag = Tag::create(["name" => "流行歌・歌謡曲", "deletable" => false]);
        Tag::create(["name" => "歌謡曲、流行歌、シャンソン、ジャズソング", "deletable" => false, "parent_tag_id" => $tag->id]);
        $tag = Tag::create(["name" => "劇音楽（大分類）", "deletable" => false]);
        Tag::create(["name" => "劇音楽", "deletable" => false, "parent_tag_id" => $tag->id]);
        $tag = Tag::create(["name" => "邦楽（大分類）", "deletable" => false]);
        Tag::create(["name" => "邦楽", "deletable" => false, "parent_tag_id" => $tag->id]);
        Tag::create(["name" => "雅楽", "deletable" => false, "parent_tag_id" => $tag->id]);
        Tag::create(["name" => "古謡", "deletable" => false, "parent_tag_id" => $tag->id]);
        Tag::create(["name" => "能楽、狂言、謡曲", "deletable" => false, "parent_tag_id" => $tag->id]);
        Tag::create(["name" => "筝曲", "deletable" => false, "parent_tag_id" => $tag->id]);
        Tag::create(["name" => "琵琶楽", "deletable" => false, "parent_tag_id" => $tag->id]);
        Tag::create(["name" => "琴楽", "deletable" => false, "parent_tag_id" => $tag->id]);
        Tag::create(["name" => "三味線楽（浄瑠璃）", "deletable" => false, "parent_tag_id" => $tag->id]);
        Tag::create(["name" => "三味線楽（地唄）", "deletable" => false, "parent_tag_id" => $tag->id]);
        Tag::create(["name" => "三味線楽（長唄）", "deletable" => false, "parent_tag_id" => $tag->id]);
        Tag::create(["name" => "三味線楽（端唄、うた沢、小唄、俗曲）", "deletable" => false, "parent_tag_id" => $tag->id]);
        Tag::create(["name" => "尺八", "deletable" => false, "parent_tag_id" => $tag->id]);
        Tag::create(["name" => "笛", "deletable" => false, "parent_tag_id" => $tag->id]);
        Tag::create(["name" => "邦楽（合奏）", "deletable" => false, "parent_tag_id" => $tag->id]);
        Tag::create(["name" => "舞踊曲（日本）", "deletable" => false, "parent_tag_id" => $tag->id]);
        $tag = Tag::create(["name" => "詩吟・朗詠", "deletable" => false]);
        Tag::create(["name" => "詩吟、朗詠", "deletable" => false, "parent_tag_id" => $tag->id]);
        $tag = Tag::create(["name" => "落語・漫才・浪曲・講談", "deletable" => false]);
        Tag::create(["name" => "落語", "deletable" => false, "parent_tag_id" => $tag->id]);
        Tag::create(["name" => "漫談、漫才", "deletable" => false, "parent_tag_id" => $tag->id]);
        Tag::create(["name" => "浪曲", "deletable" => false, "parent_tag_id" => $tag->id]);
        Tag::create(["name" => "講談", "deletable" => false, "parent_tag_id" => $tag->id]);
        $tag = Tag::create(["name" => "歌舞伎（大分類）", "deletable" => false]);
        Tag::create(["name" => "芝居囃子", "deletable" => false, "parent_tag_id" => $tag->id]);
        Tag::create(["name" => "歌舞伎", "deletable" => false, "parent_tag_id" => $tag->id]);
        $tag = Tag::create(["name" => "近代演劇（日本）", "deletable" => false]);
        Tag::create(["name" => "新派劇、新国劇", "deletable" => false, "parent_tag_id" => $tag->id]);
        Tag::create(["name" => "新劇、外国劇、放送劇、レコード劇", "deletable" => false, "parent_tag_id" => $tag->id]);
        Tag::create(["name" => "映画、テレビ", "deletable" => false, "parent_tag_id" => $tag->id]);
        $tag = Tag::create(["name" => "演劇・演芸（その他）", "deletable" => false]);
        Tag::create(["name" => "演劇、演芸", "deletable" => false, "parent_tag_id" => $tag->id]);
        $tag = Tag::create(["name" => "演説・講演・朗読・実況", "deletable" => false]);
        Tag::create(["name" => "講義、講演、演説", "deletable" => false, "parent_tag_id" => $tag->id]);
        Tag::create(["name" => "文学作品の朗読、解説", "deletable" => false, "parent_tag_id" => $tag->id]);
        Tag::create(["name" => "文学作品以外の朗読、解説", "deletable" => false, "parent_tag_id" => $tag->id]);
        Tag::create(["name" => "実況記録", "deletable" => false, "parent_tag_id" => $tag->id]);
        $tag = Tag::create(["name" => "宗教、宗教音楽", "deletable" => false]);
        Tag::create(["name" => "聖楽（キリスト教）", "deletable" => false, "parent_tag_id" => $tag->id]);
        Tag::create(["name" => "聖楽（仏教）", "deletable" => false, "parent_tag_id" => $tag->id]);
        Tag::create(["name" => "聖楽（神道）", "deletable" => false, "parent_tag_id" => $tag->id]);
        Tag::create(["name" => "経文、説話、法話", "deletable" => false, "parent_tag_id" => $tag->id]);
        $tag = Tag::create(["name" => "教育・児童", "deletable" => false]);
        Tag::create(["name" => "語学", "deletable" => false, "parent_tag_id" => $tag->id]);
        Tag::create(["name" => "体育、遊戯", "deletable" => false, "parent_tag_id" => $tag->id]);
        Tag::create(["name" => "一般教材", "deletable" => false, "parent_tag_id" => $tag->id]);
        Tag::create(["name" => "児童劇、学校劇", "deletable" => false, "parent_tag_id" => $tag->id]);
        Tag::create(["name" => "児童文芸", "deletable" => false, "parent_tag_id" => $tag->id]);
        Tag::create(["name" => "語学、体育、遊戯（運動会）", "deletable" => false, "parent_tag_id" => $tag->id]);
        $tag = Tag::create(["name" => "自然音・効果音", "deletable" => false]);
        Tag::create(["name" => " 自然音の記録", "deletable" => false, "parent_tag_id" => $tag->id]);
        Tag::create(["name" => "機械音、人工音", "deletable" => false, "parent_tag_id" => $tag->id]);
        $tag = Tag::create(["name" => "ジャンル未付与", "deletable" => false]);
        Tag::create(["name" => "不明", "deletable" => false, "parent_tag_id" => $tag->id]);
    }
}
