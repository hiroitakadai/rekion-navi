<?php

use Illuminate\Database\Seeder;

class TestSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        User::create(["name" => "Michael Example", "email" => "michael@example.com", "password" => bcrypt("password"), "admin" => true, "activated" => true, "activated_at" => Carbon::now()]);
        User::create(["name" => "Sterling Archer", "email" => "duchess@example.gov", "password" => bcrypt("password"), "activated" => true, "activated_at" => Carbon::now()]);
        User::create(["name" => "Lana Kane",       "email" => "hands@example.gov",   "password" => bcrypt("password"), "activated" => true, "activated_at" => Carbon::now()]);
        User::create(["name" => "Malory Archer",   "email" => "boss@example.gov",    "password" => bcrypt("password"), "activated" => true, "activated_at" => Carbon::now()]);

        factory(User::class, 30)->create();
    }
}
