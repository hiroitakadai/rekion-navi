<li class="list-group-item" id="commentListItem{{ $comment->id }}">
    <div class="media">
        @if (Request::is("user/*"))
            {{ Html::image(asset("img/thumbnail001.jpg"), "", ["class" => "d-flex mr-3", "width" => "80", "height" => "80"]) }}
        @else
            <a href="{{ route("user.show", $comment->user->id) }}">{!! gravatar_for($comment->user, ["size" => 50]) !!}</a>
        @endif
        <div class="media-body">
            @if (Request::is("user/*"))
                {{ Html::linkRoute("rekion.show", $comment->rekion->title, [$comment->rekion_id]) }}
            @else
                {{ Html::linkRoute("user.show", $comment->user->name, [$comment->user->id]) }}
            @endif
            {{ time_ago_in_words($comment->created_at) }}
            <br>
            {{ $comment->text }}
        @can ("delete", $comment)
            {{ Form::button("削除", ["class"=> "btn btn-outline-primary btn-sm deleteComment", "data-comment-id" => "{$comment->id}"]) }}
        @endif
        </div>
    </div>
</li>
