<ul class="list-group-flush pl-0" id="commentList">
    @each('comment.comment', $comments, "comment")
</ul>

<button type="button" id="more_item" class="btn btn-primary">もっと見る</button>

<script>
    (function($) {
        $(function () {
            let commentList = $("#commentList").children('div');
            let currentDispCount = 0;
            let maxDispCount = commentList.length;

            $("#more_item").click(function () {
                let newCount = currentDispCount + 5;

                $(commentList).each(function (i, elem) {
                    if(currentDispCount <= i && i < newCount) {
                        $(this).removeClass("d-none");
                        currentDispCount++;
                    }
                });

                if(maxDispCount<= newCount) {
                    $(this).hide();
                }

                return false;
            });

            $(commentList).each(function (i, elem) {
                $(this).addClass("d-none");
            });

            $("#more_item").click();

            $(".deleteComment").click(function () {
                if (!window.confirm('コメントを削除しますか?')) {
                    return false;
                }
                let commentId = $(this).attr("data-comment-id");
                $.ajax({
                    type: "post",
                    url: "../comment/" + commentId,
                    data: {_method: "delete"}
                }).done(function (response) {
                    $("#commentListItem" + commentId).remove();
                }).fail(function (error) {
                    console.log(error);
                });
            });
        });
    }(jQuery));
</script>
