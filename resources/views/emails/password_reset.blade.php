<h2>パスワードリセット</h2>

<p>パスワードリセットするには、次のリンクをクリックしてください。</p>

{{ Html::linkRoute("reset.edit", "Reset password", ["token" => $reset_token, "email" => $user->email]) }}

<p>このリンクは２時間後に無効になります。</p>

<p>
もし、このメールに心当たりがない場合は無視してください。
</p>
