<!DOCTYPE html>
<html>
    <head>
        <title>{{ full_title($__env->yieldContent('title')) }}</title>
        @include('layout.assets')
        @include('layout.shim')
    </head>
    <body>
        @include('layout.header')
        <div class="container-fluid pt-3 bg-white">
            <div class="row">
                <div class="col-12 col-md-3">
                    @include("layout.sidemenu")
                </div>

                <div class="col pt-3">
                    @if(session('message'))
                        @foreach (session('message') as $message_type => $message)
                            <div class="alert alert-{{ $message_type }}">{{ $message }}</div>
                        @endforeach
                    @endif
                    @yield('content')
                </div>
            </div>
        </div>
    </body>
</html>
