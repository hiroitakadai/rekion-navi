<meta name="csrf-token" content="{{ csrf_token() }}">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
{{ Html::script(mix('js/app.js'), ["media" => "all", "data-turbolinks-track" => "reload"]) }}
{{ Html::style(mix('css/app.css'), ["media" => "all", "data-turbolinks-track" => "reload"]) }}
