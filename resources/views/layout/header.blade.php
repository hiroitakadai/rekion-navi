<header class="navbar navbar-expand-md navbar-dark bg-rekion">
    <a class="navbar-brand" href="{{ url("/home") }}">
        {{ Html::image(asset("img/icon.png"), "", ["class" => "d-inline-block align-top", "width" => "28", "height" => "28"]) }}
        歴音ナビ
    </a>

    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar" aria-controls="navbar" aria-expanded="false" aria-label="Toggle something">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbar">
        <ul class="navbar-nav mr-auto">
        </ul>
        <ul class="navbar-nav">
            {{ Form::open(["method" => "get", "route" => "rekion.index", "class" => "form-inline my-2 my-sm-0"]) }}
            <div class="input-group">
                {{ Form::text("q", old("q"), ["class" => "form-control", "placeholder" => "タイトル、作者、タグ"]) }}
                <div class="input-group-append">
                    {{ Form::submit("検索", ["class" => "btn btn-dark"]) }}
                </div>
            </div>
            {{ Form::close() }}
            @if (Auth::check())
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        メニュー
                    </a>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                        {{ Html::linkRoute("user.show", "マイページ", Auth::id(), ["class"=> "dropdown-item"]) }}
                        {{ Html::linkRoute("user.edit", "アカウント設定", Auth::id(), ["class"=> "dropdown-item"]) }}
                        <div class="dropdown-divider"></div>
                        <a href="javascript:document.logoutform.submit()" class="dropdown-item my-2 my-sm-0">ログアウト</a>
                        {{ Form::open(["route" => "logout", "method" => "delete", "name" => "logoutform"]) }}
                        {{ Form::close() }}
                    </div>
                </li>
            @else
                {{ Html::linkRoute("login", "ログイン", [], ["class"=> "nav-link my-2 my-sm-0"]) }}
            @endif
        </ul>
    </div>
</header>
