<div class="accordion" id="sidemenu" role="tablist">
    <div class="card">
        <div class="card-header" role="tab" id="genreListHeading">
            <h5 class="mb-0">
                <a href="#genreList" class="collapsed text-body" role="button" data-toggle="collapse" aria-expanded="false" aria-controls="genreList">ジャンル一覧</a>
            </h5>
        </div>
        <div class="collapse show" role="tabpanel" id="genreList" aria-labelledby="genreListHeading" aria-expanded="false">
            <ul class="list-group list-group-flush">
                @foreach ($genres as $genre)
                    <li class="list-group-item d-flex justify-content-between align-items-center w-100">
                        {{ Html::linkRoute("rekion.index", $genre->name, ["tag" => $genre->id]) }}
                        <span class="badge badge-primary badge-pill">{{ $genre->rekion_count }}</span>
                    </li>
                @endforeach
            </ul>
        </div>
    </div>
    <div class="card">
        {{ Html::linkRoute("about", "このサイトについて", null, ["class"=> "nav-link"]) }}
    </div>
    <div class="card">
        <a href="http://dl.ndl.go.jp/" target="_blank" class="nav-link" rel="noopener">
            {{ Html::image(asset("img/banner_digi.gif"), "国立国会図書館デジタルコレクション(外部サイト)", ["class" => "mw-100"]) }}
        </a>
    </div>
    <div class="card">
        <a href="http://rekion.dl.ndl.go.jp" target="_blank" class="nav-link" rel="noopener">
            {{ Html::image(asset("img/banner_rekion.jpg"), "歴史的音源(外部サイト)", ["class" => "mw-100"]) }}
        </a>
    </div>
</div>
