@extends('layout.application')

@section('title', 'パスワードリセット')

@section('content')
<h2>パスワードリセット</h2>

<div class="row">
    <div class="col-md-6 col-md-offset-3">
        {{ Form::open(['route' => 'reset.store']) }}
        <div class="form-group">
            {{ Form::label('email', "メールアドレス") }}
            {{ Form::text('email', "", ["class" => "form-control"]) }}
        </div>
        <div class="form-group">
            {{ Form::submit("送信", ["class" => "btn btn-primary"]) }}
        </div>
        {{ Form::close() }}
    </div>
</div>
@endsection
