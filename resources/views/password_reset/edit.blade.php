@extends('layout.application')

@section('title', 'パスワード更新')

@section('content')
<h2>パスワード更新</h2>

<div class="row">
    <div class="col-md-6 col-md-offset-3">
        {{ Form::model($user, ["route" => ["reset.update", $token], "method" => "patch"]) }}
        @include('shared.error_messages')
        {{ Form::hidden('email', $user->email) }}
        <div class="form-group">
            {{ Form::label('password', "パスワード") }}
            {{ Form::password('password', ["class" => "form-control"]) }}
        </div>
        <div class="form-group">
            {{ Form::label('password_confirmation', "確認用パスワード") }}
            {{ Form::password('password_confirmation', ["class" => "form-control"]) }}
        </div>

        <div class="form-group">
            {{ Form::submit("送信", ["class" => "btn btn-primary"]) }}
        </div>
        {{ Form::close() }}
    </div>
</div>
@endsection
