@extends('layout.application')

@section('title', "検索結果")

@section('content')
<h2>検索結果 {{ $rekions->total() }}件</h2>

{{ $rekions->appends(["q" => old("q"), "tag" => old("tag")])->links() }}

<ul class="list-group-flush pl-0">
    @foreach ($rekions as $rekion)
        @include('rekion.rekion')
    @endforeach
</ul>

<div class="mt-3"></div>
{{ $rekions->appends(["q" => old("q"), "tag" => old("tag")])->links() }}

@endsection
