<li class="list-group-item">
    <div class="media">
        {{ Html::image(asset("img/thumbnail001.jpg"), "", ["class" => "d-flex mr-3", "width" => "80", "height" => "80"]) }}
        <div class="media-body">
            <h5 class="mt-0">{{ Html::linkRoute("rekion.show", $rekion->title, $rekion->id) }}</h5>
            {{ $rekion->artist . ", " . $rekion->maker . ", " . $rekion->publicated_at }}<br/>
            {{ $rekion->genre }}<br>
        </div>
    </div>
</li>
