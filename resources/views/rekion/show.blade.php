@extends('layout.application')

@section('title', $rekion->title)

@section('content')
<div>
    <h2>{{ $rekion->title }}</h2>
    <p class="text-muted">
        {{ $rekion->artist }}
        <br>
        {{ $rekion->maker }}
        @if ($rekion->published_at)
            / {{ $rekion->published_at }}
        @endif
        <br>
    </p>
</div>

<div class="row">
    <div class="border-left p-2 col-4 col-md-auto ml-md-3">
        再生回数<br>{{ $rekion->getViewCount() }}
    </div>
    <div class="border-left p-2 col-4 col-md-auto">
        コメント数<br>{{ $rekion->getCommentCount() }}
    </div>
    <div class="border-left p-2 col-4 col-md-auto">
        お気に入り数<br>{{ $rekion->getFavoriteCount() }}
    </div>
    @if (Auth::check() && Auth::user()->isFavorite($rekion->id))
        <div class="border-left p-2 d-flex flex-column justify-content-center col-4 col-md-auto">
            {{ Form::button("お気に入りから削除", ["class" => "btn btn-outline-secondary", "id" => "changeFavorite", "data-favorite-id" => Auth::user()->favorited->where("rekion_id", $rekion->id)->first()->id]) }}
        </div>
    @elseif (Auth::check())
        <div class="border-left p-2 d-flex flex-column justify-content-center col-6 col-md-auto">
            {{ Form::button("お気に入りに追加", ["class" => "btn btn-warning", "id" => "changeFavorite", "data-favorite-id" => 0]) }}
        </div>
    @endif
    <div class="border-left pl-2 d-flex flex-column justify-content-center col-6 col-md-auto">
        {{ Html::link($rekion->url, "歴史的音源で聞く", ["class" => "btn btn-success", "target" => "_blank"]) }}
    </div>
</div>

<div id="tagArea" class="my-1">
    @include("tag.index", ["tags" => $rekion->tags])
</div>

<div>
    <iframe src="{{ $rekion->url }}" height="300px" width="100%" id="rekionIframe" allow="autoplay" sandbox="allow-forms allow-pointer-lock allow-same-origin allow-scripts"></iframe>
</div>

<div>
    @auth
        {{ Form::text("comment", null, ["class" => "form-control mb-1", "placeholder" => "コメント", "id" => "comment"]) }}
        {{ Form::button("送信", ["class"=> "btn btn-primary", "id" => "sendComment"]) }}
    @endauth
    <div id="commentArea">
        @include("comment.index", ["comments" => $rekion->comments])
    </div>
</div>

<script>
    @auth
        $("#sendComment").on('click', function() {
            if ($("#comment").val().length == 0) {
                alert("コメントを入力して下さい。");
                return false;
            } else if ($("#comment").val().length > 140) {
                alert("コメントが長すぎます。");
                return false;
            }
            $("#sendComment").prop("disabled", true);
            $("#comment").prop("disabled", true);
            $.ajax({
                type: "post",
                url: "../comment",
                data: {
                    comment: $("#comment").val(),
                    rekion_id: {{ $rekion->id }}
                },
            }).done(function(response) {
                $("#comment").val("");
                $("#commentArea").html(response);
            }).fail(function(request) {
                console.log("sendCommentFails");
                console.log(request);
            }).always(function() {
                $("#sendComment").prop("disabled", false);
                $("#comment").prop("disabled", false);
            });
        });

        $("#changeFavorite").on('click', function() {
            $("#changeFavorite").prop("disabled", true);
            if ($(this).attr("data-favorite-id") === "0") {
                $.ajax({
                    type: "post",
                    url: "../favorite",
                    data: {
                        rekion_id: {{ $rekion->id }}
                    },
                }).done(function(response) {
                    $("#changeFavorite").removeClass("btn-warning").addClass("btn-outline-secondary");
                    $("#changeFavorite").text("お気に入りから削除");
                    $("#changeFavorite").attr("data-favorite-id", response);
                }).fail(function(request) {
                    console.log("changeFavoriteFails");
                    console.log(request);
                }).always(function() {
                    $("#changeFavorite").prop("disabled", false);
                });
            } else {
                $.ajax({
                    type: "post",
                    url: "../favorite/" + $(this).attr("data-favorite-id"),
                    data: {
                        _method: "delete"
                    },
                }).done(function(response) {
                    $("#changeFavorite").removeClass("btn-outline-secondary").addClass("btn-warning");
                    $("#changeFavorite").text("お気に入りに追加");
                    $("#changeFavorite").attr("data-favorite-id", response);
                }).fail(function(request) {
                    console.log("changeFavoriteFails");
                    console.log(request);
                }).always(function() {
                    $("#changeFavorite").prop("disabled", false);
                });
            }
        @endauth
    });
</script>

@endsection
