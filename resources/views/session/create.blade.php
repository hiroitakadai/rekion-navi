@extends('layout.application')

@section('title', 'ログイン')

@section('content')
<h2>ログイン</h2>

<div class="row">
    <div class="col-md-6 col-md-offset-3">
        {{ Form::open(['route' => 'login']) }}

        <div class="form-group">
            {{ Form::label('email', "メールアドレス") }}
            {{ Form::text('email', "", ["class" => "form-control"]) }}
        </div>
        <div class="form-group">
            {{ Form::label('password', "パスワード") }}
            {{ Html::linkRoute("reset.create", "(パスワードを忘れた)") }}
            {{ Form::password('password', ["class" => "form-control"]) }}
        </div>
        <label class="checkbox inline">
            {{ Form::hidden("remember_me", "0") }}
            {{ Form::checkbox("remember_me", "1", null, ["id" => "session_remember_me"]) }}
            <span>次回から自動的にログイン</span>
        </label>
        <div class="form-group">
            {{ Form::submit("ログイン", ["class" => "btn btn-primary"]) }}
            {{ Form::close() }}
        </div>

        <p>{{ Html::link(route("signup"), "新規登録") }}</p>
    </div>
</div>
@endsection
