@if ($errors->any())
<div id="error_explanation">
    <div class="alert alert-danger">
        {{ $errors->count() }}件のエラーがあります。
    </div>
    <ul>
    @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
    @endforeach
    </ul>
</div>
@endif
