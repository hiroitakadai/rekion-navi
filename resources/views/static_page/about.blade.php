@extends('layout.application')

@section('content')

<h2>検索について</h2>
右上の検索ボタンでキーワード検索が出来ます。<br>
スペース区切り複数のキーワードで検索することが出来ます。<br>
<br>

<h2>問い合わせ</h2>
何かありましたら hiroi.takadai(アット)gmail.com まで。

@endsection
