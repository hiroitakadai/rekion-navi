@extends('layout.application')

@section('content')
@if (!Auth::check())
    {{ Html::linkRoute("signup", "新規登録", null, ["class" => "btn btn-primary"]) }}
@endif

<h2>再生回数が多い音源</h2>

<ul class="list-group-flush pl-0">
    @foreach ($rekion_ranking as $rekion)
        @include('rekion.rekion')
    @endforeach
</ul>

<h2>最近再生された音源</h2>

<ul class="list-group-flush pl-0">
    @foreach ($rekion_histories as $rekion)
        @include('rekion.rekion')
    @endforeach
</ul>

<h2>人気の音源</h2>

<ul class="list-group-flush pl-0">
    @foreach ($rekion_favorites as $rekion)
        @include('rekion.rekion')
    @endforeach
</ul>

@endsection
