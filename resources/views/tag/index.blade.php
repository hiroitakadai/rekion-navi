<div class="d-flex flex-row">
    <div class="my-1">
        タグ
        @foreach ($tags as $tag)
            <u>{{ Html::linkRoute("rekion.index", "#".$tag->name, ["tag" => $tag->id], ["class" => "mr-2 text-nowrap"]) }}</u>
        @endforeach
    </div>
    @auth
        <button type="button" id="edit_tag" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#editTagMenu">編集</button>
    @endauth
</div>

@auth
    <div class="modal" id="editTagMenu" tabindex="-1" role="dialog" aria-labelledby="editTagMenuLabel" aria-hidden="true">
        <div class="modal-dialog modal-sm modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <div>
                        <h5 class="modal-title" id="editTagMenuLabel">タグ編集</h5>
                        ※タグの最大文字数は20文字<br>
                        ※タグの最大個数は10個
                    </div>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <ul class="list-group">
                        @foreach ($tags as $tag)
                            <li class="list-group-item d-flex justify-content-between align-items-center">
                                {{ $tag->name }}
                                @if (!$tag->deletable)
                                    <span class="badge badge-primary badge-pill">固定</span>
                                @else
                                    <button class="btn btn-primary btn-sm delete-tag" type="button" data-tag-id="{{ $tag->id }}">削除</button>
                                @endif
                            </li>
                        @endforeach
                        @if ($tags->count() < 10)
                            <li class="list-group-item d-flex justify-content-between align-items-center">
                                <div class="input-group">
                                    {{ Form::text("tag_name", null, ["class" => "form-control", "id" => "tagName"]) }}
                                    <div class="input-group-append">
                                        <button class="btn btn-primary" type="button" id="addTag">追加</button>
                                    </div>
                                </div>
                            </li>
                        @endif
                    </ul>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">閉じる</button>
                </div>
            </div>
        </div>
    </div>

    <script>
        (function($) {
            $(function () {
                $("#addTag").on("click", function(event) {
                    if ($("#tagName").val().length == 0) {
                        alert("タグ名を入力して下さい。");
                        return false;
                    } else if ($("#tagName").val().length > 20) {
                        alert("タグ名が長すぎます。");
                        return false;
                    }
                    $("#addTag").prop("disabled", true);
                    $("#tagName").prop("disabled", true);
                    $.ajax({
                        type: "post",
                        url: "../tag",
                        data: {
                            name: $("#tagName").val(),
                            rekion_id: {{ $tag->pivot->rekion_id }}
                        },
                    }).done(function(response) {
                        $("#editTagMenu").modal('hide');
                        $("#tagArea").html(response);
                    }).fail(function(error) {
                        $("#addTag").prop("disabled", false);
                        $("#tagName").prop("disabled", false);
                        console.log(error);
                    });
                });

                $(".delete-tag").on("click", function(event) {
                    $.ajax({
                        type: "post",
                        url: "../tag/" + $(this).attr("data-tag-id"),
                        data: {
                            _method: "delete",
                            rekion_id: {{ $tag->pivot->rekion_id }}
                        },
                    }).done(function(response) {
                        $("#editTagMenu").modal('hide');
                        $("#tagArea").html(response);
                        console.log(response);
                    }).fail(function(error) {
                        $("#tagArea").html(error);
                        console.log(error);
                    });
                });
            });
        }(jQuery));
    </script>

@endauth
