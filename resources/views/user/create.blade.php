@extends('layout.application')

@section('title', '新規登録')

@section('content')
<h2>新規登録</h2>

<div class="row">
    <div class="col-md-6 col-md-offset-3">
        @include("user.form", ["user"=> new App\User, "options" => ['route' => 'signup']])
    </div>
</div>
@endsection
