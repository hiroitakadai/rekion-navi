@extends('layout.application')

@section('title', 'プロフィール変更')

@section('content')
<h2>プロフィール変更</h2>

<div class="row">
    <div class="col-md-6 col-md-offset-3">
        @include("user.form", ["options" => ['route' => ['user.update', $user->id], "method" => "patch"]])

        <div class="gravatar_edit">
            {!! gravatar_for($user) !!}
            <a href="http://gravatar.com/emails" target="_blank" rel="noopener">変更</a>
        </div>
    </div>
</div>
@endsection
