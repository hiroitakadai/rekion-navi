{{ Form::model($user, $options) }}
@include('shared.error_messages')
<div class="form-group">
    {{ Form::label('name', "名前") }}
    {{ Form::text('name', $user->name, ["class" => "form-control"]) }}
</div>
<div class="form-group">
    {{ Form::label("email", "メールアドレス") }}
    {{ Form::email('email', $user->email, ["class" => "form-control"]) }}
</div>
<div class="form-group">
    {{ Form::label('password', "パスワード") }}
    {{ Form::password('password', ["class" => "form-control"]) }}
</div>
<div class="form-group">
    {{ Form::label('password_confirmation', "確認用パスワード") }}
    {{ Form::password('password_confirmation', ["class" => "form-control"]) }}
</div>

<div class="form-group">
    {{ Form::submit("送信", ["class" => "btn btn-primary"]) }}
</div>
{{ Form::close() }}
