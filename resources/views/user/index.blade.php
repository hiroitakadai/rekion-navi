@extends('layout.application')

@section('title', "ユーザー一覧")

@section('content')
<h2>ユーザー一覧</h2>

{{ $users->links() }}

<ul class="users">
    @foreach ($users as $user)
        @include('user.user')
    @endforeach
</ul>

{{ $users->links() }}

@endsection
