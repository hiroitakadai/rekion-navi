@extends('layout.application')

@section('title', $user->name)

@section('content')
<div class="row">
    <div class="col d-flex flex-row align-items-center">
        {!! gravatar_for($user) !!}
        <div class=""><h2>{{ $user->name }}</h2></div>
    </div>
</div>

<div class="row mx-1 mt-3">
    <ul class="nav nav-tabs col-12 pr-0 pr-md-3">
        <li class="nav-item col px-0 px-md-3">
            <a class="nav-link text-center px-0 px-md-3 active" href="#tab1" data-toggle="tab">お気に入り<br class="d-md-none" /> ({{ $user->favorited()->count() }})</a>
        </li>
        <li class="nav-item col px-0 px-md-3">
            <a class="nav-link text-center px-0 px-md-3" href="#tab2" data-toggle="tab">コメント<br class="d-md-none" /> ({{ $user->comments()->count() }})</a>
        </li>
        <li class="nav-item col px-0 px-md-3">
            <a class="nav-link text-center px-0 px-md-3" href="#tab3" data-toggle="tab">再生履歴<br class="d-md-none" /> ({{ $user->histories()->count() }})</a>
        </li>
    </ul>
</div>

<div class="row tab-content">
    <div id="tab1" class="tab-pane col-12 active">
        <ul class="list-group-flush pl-0">
            @foreach ($user->favoritedRekion as $rekion)
                @include('rekion.rekion')
            @endforeach
        </ul>
    </div>

    <div id="tab2" class="tab-pane col-12">
        <div id="commentArea">
            @include("comment.index", ["comments" => $user->comments])
        </div>
    </div>

    <div id="tab3" class="tab-pane col-12">
        <ul class="list-group-flush pl-0">
            @foreach ($user->historiesRekion as $rekion)
                @include('rekion.rekion')
            @endforeach
        </ul>
    </div>
</div>
@endsection
