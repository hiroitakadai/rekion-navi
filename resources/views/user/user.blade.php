<li>
    {!! gravatar_for($user, ["size" => 50]) !!}
    {{ Html::linkRoute("users.show", $user->name, [$user->id]) }}
    @if (Auth::user()->admin === true && Auth::user() != $user)
        | <a href="javascript:if(window.confirm('Yes Sure?')){document.deleteform{{$user->id}}.submit()}">delete</a>
        {{ Form::open(["route" => ["users.destroy", $user->id], "method" => "delete", "name" => "deleteform{$user->id}"]) }}
        {{ Form::close() }}
    @endif
</li>
