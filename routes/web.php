<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::redirect('/', 'home', 301);
Route::get('home', 'StaticPageController@home');
Route::get('about', 'StaticPageController@about')->name("about");

Route::get('signup', 'UserController@create')->name('signup');
Route::post('signup', 'UserController@store');
Route::resource('user', "UserController", ["except" => ["create", "store"]]);

Route::get('login', "SessionController@create")->name('login');
Route::post('login', "SessionController@store");
Route::delete('logout', "SessionController@destroy")->name('logout');

Route::get('password_reset/create', "PasswordResetController@create")->name("reset.create");
Route::post('password_reset', "PasswordResetController@store")->name("reset.store");
Route::get('password_reset/{token}/edit', "PasswordResetController@edit")->name("reset.edit");
Route::patch('password_reset/{token}', "PasswordResetController@update")->name("reset.update");

Route::resource("rekion", "RekionController", ["only" => ["index", "show"]]);

Route::resource("comment", "CommentController", ["only" => ["index", "store", "destroy"]]);

Route::resource("tag", "TagController", ["only" => ["index", "store", "destroy"]]);

Route::resource("favorite", "FavoriteController", ["only" => ["store", "destroy"]]);
